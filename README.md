# hometask-10

## Ответьте на вопросы

#### 1. Как можно сменить контекст вызова? Перечислите все способы и их отличия друг от друга.

> Ответ: С помощью функций bind, call, apply. В bind передается нужный контекст, потом аргументы через запятую, также функцию нужно вызывать руками т.е. это просто привязка контекста. При call передается контекст и аргументы через запятую, функциявызывается тут же. При apply все также, только аргументы передаются массивом.

#### 2. Что такое стрелочная функция?

> Ответ: Еще один вариант функции. Отличие в том, что у стрелочных функций нет собственного контекста. Контекст берется из внешнего окружения.

#### 3. Приведите свой пример конструктора.

```js
  function Student(name, isLazy) = {
    this.name = name;
    this.isLazy = isLazy;

    this.getName = function () {
        return this.name;
    }
  }

let student = new Student('Акакий', true);

```

#### 4. Исправьте код так, чтобы в `this` попадал нужный контекст. Исправить нужно 3мя способами, как показано в примерах урока.

```js
//bind
const person = {
    name: "Nikita",
    sayHello: function () {
        setTimeout(
            function () {
                console.log(this.name + " says hello to everyone!");
            }.bind(person),
            1000
        );
    },
};

person.sayHello();

//arrow
const person = {
    name: "Nikita",
    sayHello: function () {
        setTimeout(() => {
            console.log(this.name + " says hello to everyone!");
        }, 1000);
    },
};
person.sayHello();
// that
const person = {
    name: "Nikita",
    sayHello: function () {
        const that = this;
        setTimeout(function () {
            console.log(that.name + " says hello to everyone!");
        }, 1000);
    },
};
person.sayHello();
```

## Выполните задания

-   Установите зависимости `npm install`;
-   Допишите функции в `task.js`;
-   Проверяйте себя при помощи тестов `npm run test`;
-   Создайте Merge Request с решением.
